Available info:

Queen Dana Inspiration:
https://www.youtube.com/watch?v=PfSL-6YExzk

Album Track List:
https://www.metal-archives.com/albums/Unleash_the_Archers/Apex/635653

Album Explanation:
https://www.youtube.com/watch?v=ofm9RneJ1bs
https://www.youtube.com/watch?v=3Y1YzsVEk7s
https://www.youtube.com/watch?v=sO4N1RkdB0c


World Backstory:
https://www.metal-archives.com/bands/ancient_empire/3540383122

Pantheon origin linked to Ancient Empire:
https://pathfinder.fandom.com/wiki/Starstone
 
Dwarves:
https://die-zwerge.fandom.com/wiki/Die_Zwerge_Wiki
Axenstar - Pantheon/Star of the Dwarves
Hammerfall - The last stand of the Fifth Clan
GloryHammer - the most glorious hammer of them all
Hammer King - king of hammer
Clan of Steel - Lost Legion

Gnolls:
https://www.youtube.com/watch?v=6viXocmMzTs



























==================================================================================================
Album/Band/Artwork inspiration links:

https://www.metal-archives.com/bands/Hammer_King/3540395840

https://www.metal-archives.com/bands/Gloryhammer/3540361647

https://www.metal-archives.com/bands/Axenstar/4748

https://www.metal-archives.com/bands/Axehammer/6892 (Dwarven, in case that wasn't obvious)
Hammerfall

https://www.metal-archives.com/bands/imperial_age/3540356856

https://www.metal-archives.com/bands/Emerald_Sun/37365 (Guild)

https://www.metal-archives.com/bands/Evertale/122050 (? Warhammer lore)

https://sluglord.bandcamp.com/ (Art)

https://www.metal-archives.com/bands/Heralds_of_the_Sword/3540363241 (Interesting)

https://www.metal-archives.com/bands/Heroes_of_Forgotten_Kingdoms/3540446802 (Interesting)

https://www.metal-archives.com/bands/Kaledon/2779 (Well of resources)

https://www.metal-archives.com/bands/Antioch/3540385809 (Love)

https://www.metal-archives.com/bands/Logar%27s_Diary/1999 (Interesting)

https://www.metal-archives.com/bands/Noble_Beast/3540328165 (Decent)

https://www.metal-archives.com/bands/Steel_Attack/488 (Solid)

https://www.metal-archives.com/bands/Symphony_of_Tragedy/3540450825 (Okayish)

https://www.metal-archives.com/bands/Twilight/52084 (Art)

https://www.metal-archives.com/bands/Winterheart/42545 (Interesting)

https://www.metal-archives.com/bands/%D0%AD%D0%BA%D0%B7%D0%B8%D1%81%D1%82%D0%B5%D0%BD%D1%86%D0%B8%D1%8F/115757 (Art)

https://www.metal-archives.com/bands/Aldaria/3540422132 (Good band)

https://www.metal-archives.com/bands/Ancient_Bards/3540257762 (Art - but shit band)

https://www.metal-archives.com/bands/Arion/3540384434 (Art)

https://www.metal-archives.com/bands/Awaken_Solace/3540354319 (Interesting)

https://www.metal-archives.com/bands/Celesty/2697 (Good band)

https://www.metal-archives.com/bands/Citadel/51167 (Good logo)

https://www.metal-archives.com/bands/Fallen_Skies/17535 (1 good album)

https://www.metal-archives.com/bands/Goldenhall/3540415192 (Good name - shit band)

https://www.metal-archives.com/albums/Arwen/Illusions/60364 (Good logo)

https://www.metal-archives.com/bands/Awake/4030 (Interesting album logo)

https://www.metal-archives.com/bands/DawnRider/51272 (Art)

https://www.metal-archives.com/bands/Elwing/12253 (Good name - of course, it's from Tolkien - good art)

https://www.metal-archives.com/bands/Lost_in_Oblivion/3540285569 (Art)

https://www.metal-archives.com/bands/Lucid_Dreaming/3540368145 (Art)

https://www.metal-archives.com/bands/Lux_Perpetua/3540384432 (Art)

https://www.metal-archives.com/bands/Memorized_Dreams/8166 (Art)

https://www.metal-archives.com/bands/Moravius/82768 (Name)

https://www.metal-archives.com/bands/Nightshade/22932 (Name)

https://www.metal-archives.com/bands/Sipario/3540323601 (Art)

https://www.metal-archives.com/albums/Rushmore/Kingdom_of_Demons/532409 (?)

https://www.metal-archives.com/bands/Moriquendi/44623 (If we want to rip off Tolkien, sure)

https://www.metal-archives.com/bands/Nether_Nova/3540424266 (Terrible sofa, good art)

https://www.metal-archives.com/bands/Sythera/3540388469 (Art)

https://www.metal-archives.com/bands/Thalanos/3540396329 (Name / Art)



https://www.metal-archives.com/bands/Vh%C3%A4ldemar/5164 (Art)

https://www.metal-archives.com/bands/Scythia/3540300838 (Art)