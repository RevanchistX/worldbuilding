This simple silver ring consisted of two intertwining lattices with an inscription in Draconig along its length.

The following report was given to Axira by the scholars examining the Ring:
The simple ring seems unremarkable regarding the craftsmanship.
Considering the nature of its origin, it is believed that this was intentional,
to disguise the ring as ordinary and not cause suspicion to those looking at it.
The ring itself does not seem to have any magical properties whatsoever.
The words written on it roughly translate as ____________ in Common.
Additionally, there seems to be only five of these in existence, and they are not "aware" of each other.
The entire magic that surrounds them seems to be coming from the orb itself and not the ring.