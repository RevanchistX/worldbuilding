We have all heard the tales our elders tell, about the dawn of the world and the flower that saved the Blue Planet from imminent demise.
How the gods sacrificed themselves in an act of defiance over the malevolent Beast that sought to devour the world and feast on its core.
How evil and good joined forces together to defeat this sheer force of nature and decay that threatened the stars and every living being amongst them.
How the dragons fell into eternal slumber; how the world grew around them and they remain as the last bastion of defence against the enslaved beast.
How their songs keep the world spinning, their magic reshaping the very course of history.

What if I were to tell you that you have been taught wrong?
That these events, while they did happen, have been warped and twisted throughout the shadows of time.
A myriad of verses lost or misinterpreted in the annals of history, from soothsayer to mythweaver,
skewed and drawn out to bring about a different image,
a different tale to paint the skies red, the ground black, and the waters grey?

Let me tell you the story of the True Convocation of the Gods.
Not one you have heard at your grandsire's knee or in a tavern spewed by a drunken bard,
but of pettiness, envy, malice,
all stemming from a source of benevolence and altruism.
Or perhaps a misguided sense of justice that these so called Gods believed,
their compasses perverted by ambitions and fears, corrupted and thwarted by action and inaction alike.

Long ago, before the grandfathers of our grandfathers even walked the soil of Virrum, there was another world.
The world of Golarion, the Blue Planet.
This planet was somewhat special, for it was gifted to the Dawnflower by the Tender of Dreams.
Many of the gods set upon granting their boons onto this world,
and as time went on, it bloomed and blossomed into a true fairytale.
This was known as the Age of Creation.

Progress and tranquility blessed the primitive races of Golarion.
As per decree of its protector, Sarenrae, many a god was to mind their own musings, so long as the pervasive balance was to remain neutral.
That were she to interfere, it would have be so that the Scales of Justice, the very instrument she weighed the world's balance upon, held not even.
And an era, they did.

That is, until an ominous visage clouded the skies and a massive darkness fell upon the world.
It was Rovagug, a colossal beast escaped from the depths of the Abyss, a realm beyond realms where such beasts reside.
Its infinite hunger and insatiable appetite had led the Great Destroyer to latch its mighty jaws upon the Blue Planet,
devouring not only the raw resources, but the very cultures, history and potential energy of the world.
They do not call this beast the Worldbreaker for naught.

Thus far the tale is no doubt familiar to you, in one form or another.
But it is here when matters become more complex and therewithin lies the problem.
For the Unmaker of Worlds, while vile in his ways, was not a force of evil, directed or acting out of malice.
It was merely a trapped beast that yearned to escape, hungering for a feast after millennia of starvation. 
Upon weighing its actions many and many times more, the symmetry of the Scales remained, much to the Dawnflower's dismay.
Fury filled her veins; her hands - tied; her sense of duty, her code and principles preventing her from taking action against the creature.
Sorrow filled her heart, pleas for mercy and screams of suffering from the mortals - a constant reminder of her complacency.
Yet she did not relent, and remained true to her word and the law she decreed.

The following events are somewhat shrouded in mystery and difficult to decpher,
yet it is my belief that I have managed to untangle the web obfuscating the truth.
In her desperation, Sarenrae held a council of all the deities and alike to seek guidance on how to remedy this grave undoing.
Many divine agendas clashed - the council rendered futile and adjourned soon after, with no clear conclusion reached.

It was shortly after that Asmodeus approached her with a possible solution.
Along with his brother Ihys, they were known as The First - the initial beings willed into existence.
Ihys was, as much as such word can be used when discussing divine beings,
the father of Sarenrae and thusly, Asmodeus her uncle in this divine genealogy.
It also bears mentioning that Asmodeus, presently known as a demonic and hellish god, was at the time an angelic lord of Heaven.
How and why he acquired his current status is moot, but the timeline coincides with his return from the voyage to the Great Beyond,
where he was shared the teachings of Hell, unbeknownst to his peers.

Asmodeus brought Sarenrae a peculiar gift - a mirror decorated with a blindfolded skull perched on its ornate frame.
He confided in her that it helped him find himself, and were she to seek advice, she only need gaze upon the mirror.
It would reveal her innermost thoughts, trapped between the conscious mind and unconscious desires.
This was the counsel the Dawnflower sought, the one she yearned for - validation for her actions and guidance along the way.

The more she gazed upon the mirror, the further her thoughts began to deviate and twist beyond recognition.
Her divine feathers now turned to pure Black Wings in the glistening silver reflection.
Her countenance - sullied and decrepit - yet kindred and familiar.
In Sarenrae's mind, this was the image of her true self.
The beacon of love and justice, delivered from self-denial, turned fury and fire incarnate as she directed her wrath on the worm gnawing her planet.
The Wilted Flower evoked the judgement of the Scales of Justice once again, this time, satisfied with its outcome. 
The little justification she needed in this newfound determination was skewed by the obvious truth that escaped her eyes: it was not the Rough Beast that had tilted the Scales.
It was her and her alone.

Zealous in her righteous indignation, she summoned the council, bringing to light the shift of balance.
Empowered by her fervour, Sarenrae declared a crusade and ordained the eradication of the Beast, cleansing the malice it had wrought.
Many and many more were rallied to her cause, and just as many stood opposed, out of principle, out of spite.
Absolute war was imminent.

The details here again get murky, tarnished by the chaos of war on a global scale.
The world of Golarion was torn, ravaged, raped and pillaged by divinities and mortals alike as the hostilities continued for an aeon.

1.	The Wicked Symphony
2.	Wastelands
3.	Scales of Justice - used
4.	Dying for an Angel
5.	Blizzard on a Broken Mirror - half-used
6.	Runaway Train
7.	Crestfallen
8.	Forever Is a Long Time
9.	Black Wings - used
10.	States of Matter
11.	The Edge